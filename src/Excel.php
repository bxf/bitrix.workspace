<?php
namespace BitFactory;
/**
 * Class Excel
 * @package BitFactory
 */
class Excel {
    /**
     * Читает *.xlsx файл и возвращает массив с данными
     * @param string $strFileName - путь к файлу *.xlsx
     */
    public static function getData($strFileName='', $bDebugMode=false) {
        $strFileName = trim(str_replace('/..', '', $strFileName));
        if (!strlen($strFileName)) {
            if ($bDebugMode) {
                echo 'Не указан файл данных';
            }
            return false;
        }
        if (false === strpos($strFileName, $_SERVER['DOCUMENT_ROOT'])) {
            $strFileName = $_SERVER['DOCUMENT_ROOT'] . $strFileName;
        }
        if (!file_exists($strFileName)) {
            if ($bDebugMode) {
                echo "Файл $strFileName не найден";
            }
            return false;
        }
        if (!(@$obFile = fopen($strFileName, 'rt'))) {
            if ($bDebugMode) {
                echo "Не удалось открыть файл XSLX на чтение";
            }
            return false;
        }
        $oZip = new \ZipArchive();
        if ($oZip->open($strFileName) !== true) {
            if ($bDebugMode) {
                echo "Ошибка открытия файла";
            }
            return false;
        }
        $strTmpDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/import/tmp/';
        if (file_exists($strTmpDir)) {
            \BitFactory\Utils::removeDirectory($strTmpDir);
        }
        mkdir($strTmpDir);
        $bUnzip = $oZip->extractTo($strTmpDir);
        $oZip->close();
        if(!$bUnzip) {
            if ($bDebugMode) {
                echo 'Ошибка распаковки файла';
            }
            return false;
        }
        $arData = self::parseData($strTmpDir);

        // Чистим за собой мусор:
        @unlink($strTmpDir . '/_rels/.rels');
        \BitFactory\Utils::removeDirectory($strTmpDir);

        if (!is_array($arData) || !count($arData)) {
            if ($bDebugMode) {
                echo 'Не удалось распознать данные из файла';
            }
            return false;
        }

        return $arData;
    }
    private static function parseData($strDirName = '') {
//подгружаем названи листов
        $arResult = Array();
        $oXml = simplexml_load_file($strDirName . '/xl/workbook.xml');
        $aSheetNames = array();
        $iPos = 0;
        foreach($oXml->sheets->sheet as $rId => $oSheet) {
            $aSheetNames[++$iPos] = (string)$oSheet['name'];
        }

        //загрузка строк, используемых во всех листах
        $oXml = simplexml_load_file($strDirName . '/xl/sharedStrings.xml');
        $aSharedStrings = array();
        foreach ($oXml->children() as $oItem) {
            $aSharedStrings[] = (string)$oItem->t;
        }
        $rHandle = @opendir($strDirName . '/xl/worksheets');
        while ($sFile = @readdir($rHandle)) {
            //проходим по всем файлам из директории /xl/worksheets/
            if ($sFile != "." && $sFile != ".." && $sFile != '_rels') {
                $oXml = simplexml_load_file($strDirName . '/xl/worksheets/' . $sFile);
                //по каждой строке
                $iRow = 0;
                $iFileID = str_replace(array('sheet', '.xml'), '', $sFile);
                $sSheet = $aSheetNames[$iFileID]; //$sFile; //$aSheetNames[$iFileID];
                foreach ($oXml->sheetData->row as $oItem) {
                    $arResult[$sSheet][$iRow] = array();
                    //по каждой ячейке строки
                    $cell = 0;
                    foreach ($oItem as $oChild) {
                        $attr = (array) $oChild->attributes();
                        $attr = $attr['@attributes'];
                        $value = isset($oChild->v) ? (string)$oChild->v : false;
                        $arResult[$sSheet][$iRow][$attr['r']] = isset($attr['t']) && $attr['t'] == 's' ? $aSharedStrings[(int)$value] : $value;
                        $cell++;
                    }
                    $iRow++;
                }
                unset($oXml);
            }
        }
        return reset($arResult);
    }
}