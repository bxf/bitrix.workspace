<?php

/**
 * Class BitFactoryCore
 * Основной класс устаревшего модуля bitrix bitfactory.core
 * @deprecated В дальнейшем будем использовать \BitFactory\Utils, \BitFactory\Marketplace
 *
 * @method static BitFactory\Utils Debug($var, $name, $mode = 0, $file_name = 'log.txt')
 * @method static BitFactory\Utils SafeString($str)
 * @method static BitFactory\Utils Plural($intNum, $arForms)
 * @method static BitFactory\Utils IsEmail($email)
 * @method static BitFactory\Utils ShowFileUploadError($intError)
 * @method static BitFactory\Utils FileSize($intBytes)
 * @method static BitFactory\Utils ValueClear($str)
 * @method static BitFactory\Utils GetError()
 * @method static BitFactory\Utils ShowError()
 * @method static BitFactory\Utils TranslateFile($strPathToFile, &$arReplaces)
 * @method static BitFactory\Utils Zip($strPathToZip, $strPathToFilesDir, $strPathToFiles, &$strErrors)
 *
 * @method static BitFactory\Marketplace MakeUpdate($strModuleID, $bIncVersion, $bAutoUploadToMarketplace, $strPassword, $strLogin)
 * @method static BitFactory\Marketplace UploadUpdateToMarketplace($strModuleID, $strPathToVersion, $strPassword, $strLogin)
 *
 * @method static BitFactory\CUrl GetCookieAndSessidRequest($strURL, $arData, &$strCookie, &$strSessid)
 * @method static BitFactory\CUrl SendDataRequest($strUrl, $arData, $strCookie)
 *
 * @method static BitFactory\CBitrixComponentHelper setDefaultParams(&$arDefaultParams, &$arParams)
 * @method static BitFactory\CBitrixComponentHelper setDefaultComponentParams($obCBitrixComponent, &$arParams)
 */
class BitFactoryCore
{
    /*
     * Поиск и вызов всех старых методов в новых классах
     * */
    public static function __callStatic($strMethodName, $arArguments)
    {
        foreach (Array(
                     'Utils',
                     'Marketplace',
                     'CUrl',
                     'BitrixComponents',
                     'Zip',
                 ) as $strNewClassName) {
            if (!method_exists('BitFactory\\' . $strNewClassName, $strMethodName)) {
                continue;
            }
            return call_user_func_array("BitFactory\\$strNewClassName::$strMethodName", $arArguments);
        }
        return false;
    }
}