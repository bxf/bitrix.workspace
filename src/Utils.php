<?php
namespace BitFactory;
/**
 * Class Utils
 * @package BitFactory
 */
class Utils
{
    /**
     * @param null $var
     * @param string $name
     * @param int $mode
     * @param string $file_name
     * @return bool
     */
    public static function Debug($var = NULL, $name = '', $mode = 0, $file_name = 'log.txt')
    {
        switch ($mode) {
            case 1:
                echo '<!-- ' . $name . ' = ' . print_r($var, 1) . '-->';
                break;
            case 2:
                if ($fp = @fopen($_SERVER['DOCUMENT_ROOT'] . '/' . $file_name, 'at'))
                    if (@flock($fp, LOCK_EX)) {
                        @fputs($fp, date('Y-m-d H:i:s - ') . $name . ' = ' . print_r($var, 1) . "\n");
                        @fflush($fp);
                        @flock($fp, LOCK_UN);
                        @fclose($fp);
                    }
                break;
            /** @noinspection PhpMissingBreakStatementInspection */
            case 3:
                global $USER;
                /**@global \CUser $USER */
                if (!is_object($USER) || !$USER->IsAdmin()) {
                    return false;
                }
            /** @noinspection PhpMissingBreakStatementInspection */
            default:
                echo '<pre>' . $name . ' = ' . print_r($var, 1) . '</pre><hr>';
        }
        return true;
    }

    /**
     * @param $str
     * @return string
     */
    public static function SafeString($str)
    {
        if (is_array($str))
            foreach ($str as &$val)
                $val = self::SafeString($val);
        else
            $str = htmlspecialchars(trim($str), ENT_COMPAT, SITE_CHARSET);
        return $str;
    }

    /**
     * @param int $intNum
     * @param array $arForms
     * @return mixed
     */
    public static function Plural($intNum = 1, $arForms = Array(/**@noinspection BitrixLang*/'товар', 'товара', 'товаров'))
    {
        foreach ($arForms as &$strForm)
            $strForm = str_replace('#INT#', $intNum, $strForm);
        if (!isset($arForms[2]))
            $arForms[2] = $arForms[1];
        $intNum = abs($intNum) % 100;
        $n1 = $intNum % 10;
        if ($intNum > 10 && $intNum < 20)
            return $arForms[2];
        elseif ($n1 > 1 && $n1 < 5)
            return $arForms[1];
        elseif ($n1 == 1)
            return $arForms[0];
        return $arForms[2];
    }

    /**
     * @param $email
     * @return bool|resource
     */
    public static function IsEmail($email)
    {
        if (!preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $email))
            return FALSE;
        $strDomain = end(explode("@", $email));
        if (getmxrr($strDomain, $MXHost))
            return TRUE;
        else
            return @fsockopen($strDomain, 25, $errno, $errstr, 30);
    }

    /**
     * @param $intError
     * @return string
     */
    public static function ShowFileUploadError($intError)
    {
        switch ($intError) {
            case UPLOAD_ERR_INI_SIZE:
                return 'Размер файла превысил максимальное значение в ' . self::FileSize((int)ini_get('upload_max_filesize'));
            case UPLOAD_ERR_FORM_SIZE:
                return 'Размер файла превысил максимальное значение в ' . self::FileSize((int)$_REQUEST['MAX_FILE_SIZE']);
            case UPLOAD_ERR_PARTIAL:
                return 'Загружаемый файл был получен только частично';
            case UPLOAD_ERR_NO_FILE:
                return 'Файл не был загружен';
            case UPLOAD_ERR_NO_TMP_DIR:
                return 'Отсутствует временная папка';
            case UPLOAD_ERR_CANT_WRITE:
                return 'Не удалось записать файл на диск';
            case UPLOAD_ERR_EXTENSION:
                return 'PHP-расширение остановило загрузку файла';
            default:
                return 'Неизвестная ошибка';
        }
    }

    /**
     * @param int $intBytes
     * @return string
     */
    public static function FileSize($intBytes = 0)
    {
        $arPrefixesNames = Array('б', 'Кб', 'Мб', 'Гб', 'Tб', 'Пб');
        $intIndex = 0;
        while ($intBytes >= 1024) {
            $intBytes /= 1024;
            $intIndex++;
        }
        $intBytes = round($intBytes, 2);
        return ($intBytes . " " . $arPrefixesNames[$intIndex]);
    }

    /**
     * @param $str
     * @return string
     */
    public static function ValueClear($str)
    {
        return strip_tags(htmlspecialchars_decode($str));
    }

    /**
     * @return string
     */
    public static function GetError()
    {
        global $APPLICATION;
        $strError = '';
        if (is_object($APPLICATION) && $obExeption = $APPLICATION->GetException())
            $strError = $obExeption->GetString();
        return $strError;
    }

    /**
     *
     */
    public static function ShowError()
    {
        echo self::GetError();
    }

    /**
     * @param $strPathToFile
     * @param array $arReplaces
     */
    public static function TranslateFile($strPathToFile, &$arReplaces = Array())
    {
        file_put_contents(
            $strPathToFile,
            str_replace(
                array_keys($arReplaces),
                array_values($arReplaces),
                file_get_contents($strPathToFile)
            )
        );
    }

    /**
     * @param string $strPathToZip
     * @param string $strPathToFilesDir
     * @param string $strPathToFiles
     * @param $strErrors
     * @return int
     */
    public static function Zip($strPathToZip = '', $strPathToFilesDir = '', $strPathToFiles = '', &$strErrors)
    {
        $hZip = new \BitFactory\Zip();
        if (file_exists($strPathToZip)) {
            if ($hZip->open($strPathToZip, \ZIPARCHIVE::CHECKCONS) !== TRUE) {
                $strErrors = "Unable to Open $strPathToZip";
                return 1;
            }
        } else {
            if ($hZip->open($strPathToZip, \ZIPARCHIVE::CM_PKWARE_IMPLODE) !== TRUE) {
                $strErrors = "Could not Create $strPathToZip";
                return 1;
            }
        }
        if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $strPathToFilesDir . $strPathToFiles))
            $hZip->addDir($_SERVER['DOCUMENT_ROOT'] . '/' . $strPathToFilesDir . $strPathToFiles, $strPathToFiles);
        elseif (!$hZip->addFile($_SERVER['DOCUMENT_ROOT'] . '/' . $strPathToFilesDir . $strPathToFiles, $strPathToFiles)) {
            $strErrors = "error archiving $strPathToFiles in $strPathToZip";
            return 2;
        }
        $hZip->close();
        return 0;
    }

    public static function removeDirectory($strDirName) {
        if ($objs = glob($strDirName . "/*")) {
            foreach ($objs as $obj) {
                if ($obj == '.' || $obj == '..')
                    continue;
                is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($strDirName);
    }

    public static function Init() {
        if (empty(\Bitrix\Main\Config\Option::get('main', 'translate_key_yandex'))) {
            \Bitrix\Main\Config\Option::set('main', 'translate_key_yandex', 'trnsl.1.1.20140106T103433Z.28089e2b3975b7d6.4a858dbf7804bd237d1231350c536d9d035c93b7');
        }
    }

    public static function Deploy($filename='deploy.log') {
	    exec('cd ' . $_SERVER['DOCUMENT_ROOT'] . ' && git add . && git pull && git status', $buf);
	    file_put_contents(
		    $filename,
		    implode("\n",
			    [
				    date('Y-m-d H:i:s'),
			    ] + $buf
		    ) . "\n\n",
		    FILE_APPEND
	    );
    }

    public static function IsMobile()
    {
        foreach ([
             'iPhone',
             'iPod',
             'Android',
             'BlackBerry9530',
             'LG-TU915 Obigo', // LG touch browser
             'LGE VX',
             'webOS', // Palm Pre, etc.
             'Nokia5800',
             '2.0 MMP',
             '240x320',
             '400X240',
             'AvantGo',
             'BlackBerry',
             'Blazer',
             'Cellphone',
             'Danger',
             'DoCoMo',
             'Elaine/3.0',
             'EudoraWeb',
             'Googlebot-Mobile',
             'hiptop',
             'IEMobile',
             'KYOCERA/WX310K',
             'LG/U990',
             'MIDP-2.',
             'MMEF20',
             'MOT-V',
             'NetFront',
             'Newt',
             'Nintendo Wii',
             'Nitro', // Nintendo DS
             'Nokia',
             'Opera Mini',
             'Palm',
             'PlayStation Portable',
             'portalmmm',
             'Proxinet',
             'ProxiNet',
             'SHARP-TQ-GX10',
             'SHG-i900',
             'Small',
             'SonyEricsson',
             'Symbian OS',
             'SymbianOS',
             'TS21i-10',
             'UP.Browser',
             'UP.Link',
             'webOS', // Palm Pre, etc.
             'Windows CE',
             'WinWAP',
             'YahooSeeker/M1A1-R2D2',
        ] as $user_agent) {
            if (strpos($_SERVER["HTTP_USER_AGENT"], $user_agent) !== false) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Перегруппировывает результаты загрузки из множественных полей input:file с именами вида field_name[]
	 * @param string $field_name - имя поля в массиве $_FILES
	 * @return array
	 */
	public static function groupUploadedFiles($field_name = '')
	{
		$result = [];
		foreach ($_FILES[$field_name] as $key => $val) {
			$k = 0;
			foreach ($val as $b) {
				$result[$k][$key] = $b;
				$k++;
			}
		}
		return $result;
	}
}
