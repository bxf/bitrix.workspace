<?php

namespace BitFactory;
class Sale
{
    public static function InitEvents()
    {
        \AddEventHandler("sale", "OnOrderNewSendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
        \AddEventHandler("sale", "OnOrderStatusSendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
        \AddEventHandler("sale", "OnOrderPaySendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
        \AddEventHandler("sale", "OnOrderCancelSendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
        \AddEventHandler("sale", "OnOrderRemindSendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
        \AddEventHandler("sale", "OnOrderDeliverSendEmail", [__CLASS__, 'SetOrderLinkInEmail']);
    }

    public static function SetOrderLinkInEmail($intOrderID, &$strEventName, &$arFields)
    {
        if ((int)$intOrderID) {
            $arFields['ORDER_LINK'] = \Bitrix\Sale\Helpers\Order::getPublicLink(\Bitrix\Sale\Order::load($intOrderID));
        }
    }
}