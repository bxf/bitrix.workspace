<?php

namespace BitFactory;
/**
 * Class Marketplace
 * @package BitFactory
 */
class Marketplace
{
    /**
     * @param string $strModuleID
     * @param bool $bIncVersion
     * @param bool $bAutoUploadToMarketplace
     * @param string $strPassword
     * @param string $strLogin
     * @return bool
     */
    public static function MakeUpdate(
        $strModuleID = 'bitfactory.core',
        $bIncVersion = true,
        $bAutoUploadToMarketplace = true,
        $strPassword = '',
        $strLogin = 'marketplace@bitfactory.ru'
    )
    {
        $strModuleID = trim($strModuleID);
        $strPathToModule = $_SERVER['DOCUMENT_ROOT'] . "/local/modules/$strModuleID";
        $strBuildsDir = '/upload/builds/';
        if (!strlen($strModuleID)) {
            return false;
        }
        if (!file_exists($strPathToModule)) {
            return false;
        }
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strBuildsDir)) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . $strBuildsDir);
        }
        $arModuleVersion = Array();
        $strPathToModuleVersion = "$strPathToModule/install/version.php";
        if (file_exists($strPathToModuleVersion)) {
            include $strPathToModuleVersion;
        }
        $arVersionParts = explode('.', $arModuleVersion['VERSION']);
        foreach ($arVersionParts as &$intNum) {
            if ((int)$intNum < 0) {
                $intNum = 1;
            }
        }
        if ($bIncVersion) {
            $arVersionParts[count($arVersionParts) - 1]++; // version up;
        }
        $arModuleVersion['VERSION'] = implode('.', $arVersionParts);
        $arModuleVersion['VERSION_DATE'] = date('Y-m-d H:i:s'); // time now
        file_put_contents($strPathToModuleVersion, "<?php\n\$arModuleVersion = " . var_export($arModuleVersion, true) . ';'); // save to file
        $strPathToVersion = $_SERVER['DOCUMENT_ROOT'] . $strBuildsDir . $arModuleVersion['VERSION'];
        CopyDirFiles(
            $strPathToModule,
            $strPathToVersion,
            true,
            true,
            false,
            'builds'
        );
        \BitFactory\Utils::Zip(
            $strPathToVersion . '.zip',
            $strBuildsDir,
            $arModuleVersion['VERSION'],
            $strError
        );
        DeleteDirFilesEx($strBuildsDir . $arModuleVersion['VERSION']);
        if ($bAutoUploadToMarketplace) {
            self::UploadUpdateToMarketplace(
                $strModuleID,
                $strPathToVersion . '.zip',
                $strPassword,
                $strLogin
            );
        }
        @unlink($strPathToVersion . '.zip');
        return true;
    }

    /**
     * @param string $strModuleID
     * @param string $strPathToVersion
     * @param string $strPassword
     * @param string $strLogin
     * @return bool
     */
    public static function UploadUpdateToMarketplace(
        $strModuleID = 'bifactory.core',
        $strPathToVersion = '',
        $strPassword = '',
        $strLogin = 'marketplace@bitfactory.ru'
    )
    {
        $strModuleID = trim($strModuleID);
        if (!strlen($strModuleID)) {
            return false;
        }
        if (!file_exists($strPathToVersion)) {
            return false;
        }
        if (!strlen($strPassword)) {
            return false;
        }
        $strUrl = 'http://partners.1c-bitrix.ru/personal/modules/edit_update_module.php?module=' . $strModuleID;
        \BitFactory\CUrl::GetCookieAndSessidRequest(
            $strUrl,
            Array(
                'module'        => $strModuleID,
                'login'         => 'yes',
                'AUTH_FORM'     => 'Y',
                'TYPE'          => 'AUTH',
                'USER_LOGIN'    => $strLogin,
                'USER_PASSWORD' => $strPassword,
            ),
            $strCookie,
            $strSessid
        );
        $arFields = Array(
            'module' => $strModuleID,
            'update' => "@" . $strPathToVersion,
            'sessid' => $strSessid,
            'submit' => ' ',
        );
        \BitFactory\CUrl::SendDataRequest(
            $strUrl,
            $arFields,
            $strCookie
        );
        ?>
        <META HTTP-EQUIV="REFRESH" CONTENT="0;URL='http://partners.1c-bitrix.ru/personal/modules/update_module.php?ID=<?= $strModuleID ?>"><?
        return true;
    }

    /**
     * @param string $strModuleID
     * @return bool
     */
    public static function ReInstallModule($strModuleID = 'bitfactory.core')
    {
        $strModuleID = trim($strModuleID);
        if (!strlen($strModuleID)) {
            return false;
        }
        $strPathToModule = $_SERVER['DOCUMENT_ROOT'] . BX_ROOT . "/modules/$strModuleID/install/index.php";
        if (!file_exists($strPathToModule)) {
            return false;
        }
        include $strPathToModule;
        $strModuleClassName = str_replace('.', '_', $strModuleID);
        /**@var \CModule $obModule*/
        $obModule = new $strModuleClassName();
        if (IsModuleInstalled($strModuleID)) {
            $obModule->DoUninstall();
        }
        $obModule->DoInstall();
        return true;
    }
}