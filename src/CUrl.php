<?php

namespace BitFactory;
/**
 * Class CUrl
 * @package BitFactory
 */
class CUrl
{
    /**
     * @param string $strURL
     * @param array $arData
     * @param $strCookie
     * @param $strSessid
     * @return bool
     */
    public static function GetCookieAndSessidRequest($strURL = '', $arData = Array(), &$strCookie, &$strSessid)
    {
        if (!strlen($strURL)) {
            return false;
        }
        $hCurl = curl_init($strURL);
        curl_setopt($hCurl, CURLOPT_HEADER, true);
        curl_setopt($hCurl, CURLOPT_NOBODY, true);
        if (strlen($strCookie) > 0) {
            curl_setopt($hCurl, CURLOPT_COOKIE, $strCookie);
        }
        if (is_array($arData) && count($arData)) {
            curl_setopt($hCurl, CURLOPT_POST, true);
            curl_setopt($hCurl, CURLOPT_POSTFIELDS, $arData);
        }
        ob_start();
        curl_exec($hCurl);
        curl_close($hCurl);
        $strBuf = ob_get_contents();
        ob_end_clean();
        preg_match('/<input(.*)name="sessid"(.*)value="(.+)" \/>/', $strBuf, $arResult);
        $strSessid = $arResult[3];
        preg_match_all('/(?<=Set-Cookie: ).[a-zA-Z0-9_=]+/', $strBuf, $arResult);
        $strCookie .= implode("\n", $arResult[0]);
        return true;
    }

    /**
     * @param string $strURL
     * @param array $arData
     * @param string $strCookie
     * @return bool
     */
    public static function SendDataRequest($strURL = '', $arData = Array(), $strCookie = '')
    {
        if (!strlen($strURL)) {
            return false;
        }
        $hCurl = curl_init($strURL);
        curl_setopt($hCurl, CURLOPT_HEADER, true);
        curl_setopt($hCurl, CURLOPT_REFERER, $strURL);
        curl_setopt($hCurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.63 Safari/537.31');
        curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);

        if (strlen($strCookie) > 0) {
            curl_setopt($hCurl, CURLOPT_COOKIE, $strCookie);
        }

        if (is_array($arData) && count($arData)) {
            curl_setopt($hCurl, CURLOPT_POST, true);
            curl_setopt($hCurl, CURLOPT_POSTFIELDS, $arData);
        }

        if (!curl_exec($hCurl)) {
            echo curl_error($hCurl) . '(' . curl_errno($hCurl) . ')';
        }
        curl_close($hCurl);
        return true;
    }
}