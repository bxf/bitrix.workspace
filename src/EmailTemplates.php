<?php
/**
 * Автоматизация настроек почтовых шаблонов
 */
namespace BitFactory;
class EmailTemplates
{
	/**
	 * Метод устанавливает красивое поле отправителя
	 */
	public static function resetFromField()
	{
		$res = \CEventMessage::GetList(
			($by = 'id'),
			($order = 'asc'),
			[]
		);
		$em = new \CEventMessage();
		while ($template = $res->Fetch()) {
			if (!in_array(
				$template['EMAIL_FROM'],
				[
					'#DEFAULT_EMAIL_FROM#',
					'#SALE_EMAIL#',
				]
			)) {
				continue;
			}
			$em->Update(
				$template['ID'],
				[
					'EMAIL_FROM' => '#SITE_NAME# <' . $template['EMAIL_FROM'] . '>',
				]
			);
		}
	}

	public static function resetSubject() {
		$res = \CEventMessage::GetList(
			($by = 'id'),
			($order = 'asc'),
			[]
		);
		$em = new \CEventMessage();
		while ($template = $res->Fetch()) {
			$template['SUBJECT'] = str_replace(
				[
					'#SITE_NAME#: ',
				],
				[
					'',
				],
				$template['SUBJECT']
			);
			if (false !== strpos($template['EVENT_NAME'], 'SALE_')) {
				$template['SUBJECT'] = 'Заказ №#ORDER_ID#';
			}
			$em->Update(
				$template['ID'],
				[
					'SUBJECT' => $template['SUBJECT'],
				]
			);
		}
	}
}