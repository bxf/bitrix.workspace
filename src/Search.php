<?php

namespace BitFactory;
/**
 * Дополнения для работы с модулем поиска
 * Class Search
 * @package BitFactory
 */
class Search
{
	private static $interval = 3600;
	/**
	 * Функция агента переиндексации
	 * @return string
	 */
	public static function ReIndex()
	{
		if (\Bitrix\Main\Loader::includeModule('search')) {
			$result = false;
			do {
				$result = \CSearch::ReIndexAll(true, 600, $result);
			} while (is_array($result));
		}
		return "\\" . __METHOD__ . "();";
	}

	/*
	 * Регистрация агента
	 */
	public static function Install()
	{
		\CAgent::RemoveAgent("\\" . __CLASS__ . '::ReIndex();');
		\CAgent::AddAgent("\\" . __CLASS__ . '::ReIndex();', 'search', 'Y', self::$interval);
	}
}