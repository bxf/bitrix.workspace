<?php

namespace BitFactory;
/**
 * Class Zip
 * @package BitFactory
 */
class Zip extends \ZipArchive
{
    /** Добавление сразу папки в архив
     * @param $location
     * @param $name
     */
    public function addDir($location, $name)
    {
        $this->addEmptyDir($name);
        $this->addDirDo($location, $name);
    } // EO addDir;

    private function addDirDo($location, $name)
    {
        $name .= '/';
        $location .= '/';

        $dir = opendir($location);
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..') continue;
            $do = (filetype($location . $file) == 'dir') ? 'addDir' : 'addFile';
            $this->$do($location . $file, $name . $file);
        }
    } // EO addDirDo();
}