<?php

namespace BitFactory;
/**
 * Class CBitrixComponentHelper
 * @package BitFactory
 */
class CBitrixComponentHelper
{
    public static function setDefaultParams(&$arDefaultParams, &$arParams)
    {
        if (!is_array($arDefaultParams) || empty($arDefaultParams))
            return false;
        foreach ($arDefaultParams as $strCode => $arParameter) {
            if (!isset($arParams[$strCode]) && isset($arParameter['DEFAULT']))
                $arParams[$strCode] = $arParameter['DEFAULT'];
        }
        return true;
    }
    /**@todo Решить проблему с RELOAD свойствами */
    /**@todo Решить проблему PHP кодом в значениях свойств */
    public static function setDefaultComponentParams($obCBitrixComponent, &$arParams)
    {
        /**@var \CBitrixComponent $obCBitrixComponent */
        $strComponentParametersFilePath = $_SERVER['DOCUMENT_ROOT'] . $obCBitrixComponent->getPath() . '/.parameters.php';
        if (file_exists($strComponentParametersFilePath)) {
            IncludeModuleLangFile($strComponentParametersFilePath);
            include $strComponentParametersFilePath;
            /**@var array $arComponentParameters */
            self::setDefaultParams($arComponentParameters['PARAMETERS'], $arParams);
        }

        if (!$obComponentTemplate = $obCBitrixComponent->getTemplate()) {
            $obCBitrixComponent->initComponentTemplate();
            $obComponentTemplate = $obCBitrixComponent->getTemplate();
        }
        $strComponentTemplateParameters = $_SERVER['DOCUMENT_ROOT'] . $obComponentTemplate->GetFolder() . '/.parameters.php';
        /**@var array $arTemplateParameters */
        if (file_exists($strComponentTemplateParameters)) {
            IncludeModuleLangFile($strComponentTemplateParameters);
            include $strComponentTemplateParameters;
            /**@var array $arTemplateParameters */
            self::setDefaultParams($arTemplateParameters, $arParams);
        }
    }
}