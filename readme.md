# Набор классов и инструментов для 1С-Битрикс
## Установка с помощью Composer
Объявляем зависимость:

```
#!bash

composer require bitfactory/bitrix.workspace dev-master
```

И дополнительно прописываем параметры:
```
#!json

{
    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:bxf/bitrix.workspace.git"
        }
    ]
}
```